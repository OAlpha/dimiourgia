class UserMailer < ActionMailer::Base
  default from: 'lnugentgibson@gmail.com'
 
  def welcome_email(user, url)
    @user = user
    salted = @user.passhash
    hash = Digest::MD5.hexdigest(Digest::MD5.hexdigest(@user.email)+salted)
    url  = url + '/login?' + hash
    mail(to: @user.email, subject: 'Welcome to Dimiourgia!')
  end
end

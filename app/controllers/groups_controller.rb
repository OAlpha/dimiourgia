class GroupsController < ApplicationController
  def create
    phash = {}
    phash[:groupname] = params[:group][:groupname]
    phash[:permissions] = params[:group][:permissions]
    @group = Group.new(phash)
 
    respond_to do |format|
      if @group.save
        format.html { redirect_to(@group, notice: 'Group was successfully created.') }
        format.json { render json: @group, status: :created, location: @group }
      else
        format.html { render action: 'new' }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @group = Group.find(params[:id])
  end

  def destroy
    @group = Group.find(params[:id])
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_url, notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def index
    @groups = Group.all
  end

  def edit
  end

  def new
    @group = Group.new
  end

  def update
  end
end

class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.first(username: params[:user][:username])
    if @user.nil?
    
    phash = {}
    phash[:email] = params[:user][:email]
    phash[:username] = params[:user][:username]
    phash[:firstname] = params[:user][:firstname]
    phash[:middlename] = params[:user][:middlename]
    phash[:lastname] = params[:user][:lastname]
    phash[:salt] = 'salt'
    if( params[:hashed] == 'true' )
      phash[:passhash] = Digest::MD5.hexdigest(params[:user][:passhash]+Digest::MD5.hexdigest(phash[:salt]))
    else
      phash[:passhash] = Digest::MD5.hexdigest(Digest::MD5.hexdigest(params[:password])+Digest::MD5.hexdigest(phash[:salt]))
    end
    # generate salt
    phash[:group] = Group.find(1)
    @user = User.new(phash)
 
    respond_to do |format|
      if @user.save
        # Tell the UserMailer to send a welcome email after save
        UserMailer.welcome_email(@user, request.domain).deliver
 
        format.html { redirect_to(@user, notice: 'User was successfully created.') }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
    
    else
        format.html { redirect_to(new_user_path, notice: 'User with that name already exists.') }
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def showuser
    @user = User.first(params[username: params[:username]])
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def index
    @users = User.all
  end

  def edit
  end

  def update
  end
end

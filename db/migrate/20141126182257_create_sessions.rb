class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.references :user
      t.text :user_agent
      t.timestamp :login
      t.timestamp :last_action
    end
  end
end

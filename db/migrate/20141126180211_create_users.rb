class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.index :username, :unique => true
      t.string :email
      t.index :email, :unique => true
      t.references :group
      t.string :salt
      t.string :passhash
      t.integer :perm_override_remove
      t.integer :perm_override_add
      t.datetime :registered
      t.timestamp :last_login
      t.integer :reg_ip
      t.integer :last_login_ip
      t.boolean :must_validate
    end
  end
end

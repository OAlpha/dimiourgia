class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :groupname
      t.index :groupname, :unique => true
      t.integer :permissions
    end
  end
end

class CreateAutologins < ActiveRecord::Migration
  def change
    create_table :autologins do |t|
      t.references :user
      t.string :public_key
      t.string :private_key
      t.timestamp :created
      t.timestamp :last_used
      t.integer :last_used_ip
    end
  end
end

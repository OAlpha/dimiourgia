# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141126201515) do

  create_table "autologins", force: true do |t|
    t.integer  "user_id"
    t.string   "public_key"
    t.string   "private_key"
    t.datetime "created"
    t.datetime "last_used"
    t.integer  "last_used_ip"
  end

  create_table "groups", force: true do |t|
    t.string  "groupname"
    t.integer "permissions"
  end

  add_index "groups", ["groupname"], name: "index_groups_on_groupname", unique: true

  create_table "sessions", force: true do |t|
    t.integer  "user_id"
    t.text     "user_agent"
    t.datetime "login"
    t.datetime "last_action"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "email"
    t.integer  "group_id"
    t.string   "salt"
    t.string   "passhash"
    t.integer  "perm_override_remove"
    t.integer  "perm_override_add"
    t.datetime "registered"
    t.datetime "last_login"
    t.integer  "reg_ip"
    t.integer  "last_login_ip"
    t.boolean  "must_validate"
    t.string   "firstname"
    t.string   "middlename"
    t.string   "lastname"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
